package com.ELCyrus.pma.services;

import org.springframework.stereotype.Service;

@Service
//Repository
//Component

// Types of injection

//- Field Injection
//- Constructor Injection
// For constructor injection we do not need an @Autowired
// - setter injection
// - Spring knows with annotation @Controller

public class DataCleasingService {

	public DataCleasingService() {
		super();
		
	}
	
}
