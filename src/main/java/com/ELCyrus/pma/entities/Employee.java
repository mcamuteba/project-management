package com.ELCyrus.pma.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Employee {
	
	@Id
	@GenericGenerator(
		      name = "employee_seq_generator", strategy = "sequence", parameters = {
		      @org.hibernate.annotations.Parameter(name = "increment_size", value = "1"),
		      @org.hibernate.annotations.Parameter(name = "sequence_name", value = "employee_seq")
		})
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="employee_seq")// use identity instead of Auto
	@SequenceGenerator(name = "employee_seq", sequenceName = "employee_seq",
    allocationSize = 1,initialValue=1)
	
	
	//@GeneratedValue(strategy=GenerationType.AUTO)
	private long employee_ID;
	private String firstName;
	private String lastName;
	private String email;
	
	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST},
			fetch=FetchType.LAZY)
	@JoinTable(name="project_employee",
	joinColumns=@JoinColumn(name="employee_id"),
	inverseJoinColumns= @JoinColumn(name="project_id")
)
	@JoinColumn(name="project_id")
	
	
	private List<Project> Projects;
	
	public Employee() {
		
	}
	
	
	public Employee(String firstName, String lastName, String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}


	public long getEmployeeID() {
		return employee_ID;
	}
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	public List<Project> getProjects() {
		return Projects;
	}


	public void setProjects(List<Project> projects) {
		Projects = projects;
	}





	
	
	

}
