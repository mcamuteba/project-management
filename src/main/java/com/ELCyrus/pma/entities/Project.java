package com.ELCyrus.pma.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Project {
	
	@Id
	@GenericGenerator(
		      name = "project_seq_generator", strategy = "sequence", parameters = {
		      @org.hibernate.annotations.Parameter(name = "increment_size", value = "1"),
		      @org.hibernate.annotations.Parameter(name = "sequence_name", value = "project_seq")
		})
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="project_seq")
	@SequenceGenerator(name = "project_seq", sequenceName = "project_seq",
    allocationSize = 1,initialValue=1)
	
//	@GeneratedValue(strategy=GenerationType.AUTO)
	private long projectId;
	private String name;
	private String stage;
	private String description;
	
	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST},
			fetch=FetchType.LAZY)
	@JoinTable(name="project_employee",
			joinColumns=@JoinColumn(name="project_id"),
			inverseJoinColumns= @JoinColumn(name="employee_id")
	)
	
	private List<Employee> employees;
	
	public Project() {
		
	}
	
	
	public Project(String name, String stage, String description) {
		super();
		this.name = name;
		this.stage = stage;
		this.description = description;
	}
	
	
	public List<Employee> getEmployees() {
		return employees;
	}


	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}


	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}


	public long getProjectId() {
		return projectId;
	}

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public void addEmployee(Employee emp) {
		// TODO Auto-generated method stub
		
		if(employees == null) {
			employees = new ArrayList<>();
		}
		
	 employees.add(emp);
		
	}

	
	
	
}
