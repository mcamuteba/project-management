package com.ELCyrus.pma.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ELCyrus.pma.dto.ChartData;
import com.ELCyrus.pma.entities.Project;


public interface ProjectRepository extends CrudRepository<Project, Long> {

	@Override
	public List<Project> findAll();
	
	@Query(nativeQuery = true, 
	value="SELECT stage as label, count(*) as value"
			+ " From project "
			+ "GROUP BY Stage")
	public List<ChartData> getProjectStatus();
}
