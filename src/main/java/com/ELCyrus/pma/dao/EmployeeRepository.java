package com.ELCyrus.pma.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ELCyrus.pma.dto.EmployeeProject;
import com.ELCyrus.pma.entities.Employee;


public interface EmployeeRepository extends CrudRepository<Employee, Long> {

	@Override
	public List<Employee> findAll();
	
	
	@Query(nativeQuery=true, value="select E.FIRST_NAME as firstName, E.EMAIL as lastName, count(PE.EMPLOYEE_ID ) as projectCount"
			+ " from EMPLOYEE as E "
			+ "LEFT JOIN PROJECT_EMPLOYEE as PE "
			+ "ON E.EMPLOYEE_ID = PE.EMPLOYEE_ID "
			+ "GROUP BY E.FIRST_NAME , E.EMAIL  ")
	
	public List<EmployeeProject> employeeProjects();
}
