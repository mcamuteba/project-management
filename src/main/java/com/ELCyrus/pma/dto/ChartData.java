package com.ELCyrus.pma.dto;

public interface ChartData {

	public String getLabel();
	public long getValue();
	
}
