package com.ELCyrus.pma.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.ELCyrus.pma.dao.EmployeeRepository;
import com.ELCyrus.pma.dao.ProjectRepository;
import com.ELCyrus.pma.dto.ChartData;
import com.ELCyrus.pma.dto.EmployeeProject;
import com.ELCyrus.pma.entities.Project;
import com.ELCyrus.pma.springExample.Car;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller

public class HomeController {

	@Value("${version}")
	private String versionNumber;
	
	@Autowired
	Car car;
	
	@Autowired
	ProjectRepository proRepo;
	
	@Autowired
	EmployeeRepository empRepo;
	
	@GetMapping("/")
	public String displayHome (Model model) throws JsonProcessingException {
		
		model.addAttribute("versionNumber", versionNumber);
		
		
		Map<String, Object> map = new HashMap<>();
			
		List<Project> projects = proRepo.findAll();
		model.addAttribute("projectsList", projects);
		
		
		
		List<ChartData> projectData = proRepo.getProjectStatus();
		ObjectMapper objectMapper = new ObjectMapper();
		
		String jsonString = objectMapper.writeValueAsString(projectData);
		
		
		model.addAttribute("projectStatusCnt", jsonString);
		
		
		List<EmployeeProject> employeesProjectCnt = empRepo.employeeProjects();
		model.addAttribute("employeesListProjectsCnt", employeesProjectCnt);
		
		return "Main/home";
	}
}
