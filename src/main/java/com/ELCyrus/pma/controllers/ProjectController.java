package com.ELCyrus.pma.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ELCyrus.pma.dao.EmployeeRepository;
import com.ELCyrus.pma.dao.ProjectRepository;
import com.ELCyrus.pma.entities.Employee;
import com.ELCyrus.pma.entities.Project;

@Controller
@RequestMapping("/projects")
public class ProjectController {
	
	@Autowired
	ProjectRepository proRepo;

	@Autowired
	EmployeeRepository empRepo;
	
	@GetMapping
	public String displayEmployees(Model model) {
		
		List<Project> projects = proRepo.findAll();
		model.addAttribute("projects", projects);
		
		return "projects/list-projects";
	}
	
	//this is a new comment to check the git repo
	
	@GetMapping("/new")
	public String displayProjectForm(Model model) {
		
		Project  aProject = new Project();
		List<Employee> employees = empRepo.findAll();
		
		model.addAttribute("project", aProject);
		model.addAttribute("allEmployees", employees);
		return "projects/new-project";
	}
	
	@PostMapping("/save")
	public String createProject(Project project,
								//@RequestParam List<Long> employees, 
								Model model) {
		proRepo.save(project);
		
		//TODO: Replace this redirect by another one that can prevent 
		//TODO: duplicate submission
		return "redirect:/projects/new";
	}
	
	//jdbc:h2:~/test
	
}
