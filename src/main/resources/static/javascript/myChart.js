/*const config =  {
  type: 'doughnut',
  data: data,
  options: {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: 'Chart.js Doughnut Chart'
      }
    }
  },
};*/

var chartData = decodeHtml(chartData);
var chartJsonArray = JSON.parse(chartData);

var arrayLength = chartJsonArray.length;

var numericData = [];
var labelData = [];

for(var i=0; i <arrayLength; i++){
	
	numericData[i] = chartJsonArray[i].value;
	labelData[i] =  chartJsonArray[i].label;
}

var ctx = document.getElementById('myPieChart');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: labelData,
        datasets: [{
            label: '# of Votes',
            data: numericData,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)'
            ],
            borderColor: 
                'rgba(200, 50, 101, 1)',
            borderWidth: 1,
            data:numericData
        }]
    },
    options: {
       title: {
			display: true,
			text: 'Project Statuses'
}
    }
});


function decodeHtml(html) {
	var txt = document.createElement("textarea");
	txt.innerHTML = html;
	return txt.value;
}