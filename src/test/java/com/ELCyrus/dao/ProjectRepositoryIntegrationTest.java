package com.ELCyrus.dao;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit.jupiter.SpringExtension;
//import static org.junit.

import com.ELCyrus.pma.ProjectManagementApplication;
import com.ELCyrus.pma.dao.ProjectRepository;
import com.ELCyrus.pma.entities.Project;

@ContextConfiguration(classes=ProjectManagementApplication.class)
@ExtendWith(SpringExtension.class)
@SqlGroup({@Sql(executionPhase=ExecutionPhase.BEFORE_TEST_METHOD, scripts= {"classpath:schema.sql", "classpath:import.sql"}),
			@Sql(executionPhase=ExecutionPhase.AFTER_TEST_METHOD, scripts= "drop.sql")})
@DataJpaTest
public class ProjectRepositoryIntegrationTest {
	

	@Autowired
	ProjectRepository proRepo;
	
	@Test
	public void ifNewProjectSave_thenSuccess() {
		Project newProject = new Project("New Test Project", "COMPLETE", "Test description");
		proRepo.save(newProject);
				
		
//		assertEquals(5, proRepo.findAll().size());
		//(5, proRepo.findAll().size());
	
	}
}
